name := "Torree"

version := "0.1"

scalaVersion := "2.11.7"

connectInput in run := true

libraryDependencies += "org.scalatest" %% "scalatest" % "2.1.6" % "test"

libraryDependencies += "org.clapper" %% "argot" % "1.0.4"

libraryDependencies += "io.netty" % "netty-all" % "4.0.4.Final"

libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.4.1"

enablePlugins(GitBranchPrompt)
