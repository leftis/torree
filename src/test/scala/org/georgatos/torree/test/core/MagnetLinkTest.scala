package org.georgatos.torree.test.core

import org.georgatos.torree.core.MagnetLink
import org.georgatos.torree.exceptions._
import org.georgatos.torree.test.Test
import org.scalatest.BeforeAndAfter

class MagnetLinkTest extends Test with BeforeAndAfter {

  val validMagnetLink = MagnetLink("magnet:?xt=urn:btih:195DEF3279F00252E5B4B6E58058B837A9EFBA5D&dn=arrow+s04e04+hdtv+x264+lol+ettv&tr=udp%3A%2F%2Ftracker.publicbt.com%2Fannounce&tr=udp%3A%2F%2Fopen.demonii.com%3A1337")
  val invalidMagnetLink = MagnetLink("invalid string")

  describe("MagnetLink") {
    describe("valid") {
      describe("#trackers") {
        it("should return a list of the trackers") {
          validMagnetLink.trackers should be(Some(List("udp%3A%2F%2Ftracker.publicbt.com%2Fannounce", "udp%3A%2F%2Fopen.demonii.com%3A1337")))
        }
      }

      describe("#name") {
        it("should return the name the name of the torrent") {
          validMagnetLink.name should be(Some("arrow s04e04 hdtv x264 lol ettv"))
        }
      }

      describe("#valid") {
        it("should return true whre the string is valid") {
          validMagnetLink.valid should equal(true)
        }
      }
    }

    describe("invalid") {
      describe("#valid") {
        it("should return false when the string is invalid") {
          invalidMagnetLink.valid should equal(false)
        }
      }

      describe("#name") {
        it("should raise exception") {
          intercept[InvalidMagnetString] {
            invalidMagnetLink.name
          }
        }
      }
    }
  }
}