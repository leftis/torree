package org.georgatos.torree.test.core

import org.georgatos.torree.core._
import org.georgatos.torree.test.Test
import org.scalatest.BeforeAndAfter

class TrackerAddressTest extends Test with BeforeAndAfter {
  val httpAnnounceUrl = "http://blackcat.filers.ru:1337/announce"
  val udpAnnounceUrl = "udp://open.demonii.com:1337/announce"
  val encodedHttpAnnounceUrl = "http%3A%2F%2Fblackcat.filers.ru%3A1337%2Fannounce"

  describe("TrackerAddress") {
    it("should return an instance of httpTrackerAddress") {
      TrackerAddress(httpAnnounceUrl) shouldBe a [TrackerAddress[HTTP]]
    }

    it("should return an instance of udpTrackerAddress") {
      TrackerAddress(udpAnnounceUrl) shouldBe a [TrackerAddress[UDP]]
    }

    it("should return the address decoded") {
//      val newAddress = TrackerAddress(httpAnnounceUrl)
//      newAddress.add
    }
  }
}