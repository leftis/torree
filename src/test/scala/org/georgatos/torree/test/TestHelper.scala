package org.georgatos.torree.test

import org.scalatest._

abstract class Test extends FunSpec with Matchers with OptionValues with Inside with Inspectors