package org.georgatos.torree.test

import org.georgatos.torree.core.MagnetLink
import org.georgatos.torree.exceptions._
import org.georgatos.torree.parsers.MagnetParser
import scala.collection.immutable.HashMap


class MagnetParserSpec extends Test {
  describe("A MagnetPaser") {
    it("should raise on invalid magnet") {
      intercept[InvalidMagnetString] {
        MagnetParser(MagnetLink("invalid string"))
      }
    }

    it("should return a parsed object when the link is valid") {
      MagnetParser(
        MagnetLink("magnet:?xt=urn:btih:195DEF3279F00252E5B4B6E58058B837A9EFBA5D&dn=arrow+s04e04+hdtv+x264+lol+ettv&tr=udp%3A%2F%2Ftracker.publicbt.com%2Fannounce&tr=udp%3A%2F%2Fopen.demonii.com%3A1337")
      ) shouldBe a [Some[HashMap[String, List[String]]]]
    }
  }
}