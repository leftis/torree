package org.georgatos.torree.util

import scala.util.Try

object Decode {
  import java.net.URLDecoder
  import java.nio.charset.Charset

  trait Extract {
    def charset: Charset
    def apply(raw: String) = Try(URLDecoder.decode(raw, charset.name())).toOption
    def unapply(raw: String) = Try(URLDecoder.decode(raw, charset.name())).toOption
  }

  object utf8 extends Extract {
    val charset = Charset.forName("utf8")
  }
}