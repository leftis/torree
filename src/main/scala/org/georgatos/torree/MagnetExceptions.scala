package org.georgatos.torree.exceptions

case class InvalidMagnetString(message: String) extends Exception(message)