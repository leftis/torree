package org.georgatos.torree.core

import java.net.InetAddress
import io.netty.bootstrap.{Bootstrap}
import io.netty.channel.{Channel, ChannelHandler, ChannelInitializer, ChannelOption}
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.nio.{NioDatagramChannel}

private class Tracker(protected val trackers: Option[List[String]]) {
  /**
    * Announce url
    */
  private val announceURL = "/announce"

  /**
    *  Return inet addresses for trackers
    */
//  private val inetAddressesOfTrackers: List[Option[InetAddress]] =
//    trackers.get.map(tr => toInetAdress(tr))

//  def toInetAdress(str: String): Option[InetAddress] = {
//    case InetAddress.getByName(str).isReachable(10) => x
//    case _ => None
//  }

  /**
    *  Nio event group
    */
  val nioEventLoopGroup = new NioEventLoopGroup

  /**
    * Nio datagram channel
    */
  val channel = new NioDatagramChannel


  def createBootstrap(handler: ChannelHandler) = new Bootstrap()
    .group(nioEventLoopGroup)
    .channel(classOf[NioDatagramChannel])
    .option[java.lang.Boolean](ChannelOption.SO_KEEPALIVE, true)
    .handler(new ChannelInitializer[io.netty.channel.Channel] {
      def initChannel(ch: Channel): Unit = {
        ch.pipeline().addLast(nioEventLoopGroup, handler)
      }
    })

  def announce = {
  }
}

object Tracker {
  def apply(trackers: Option[List[String]]) = new Tracker(trackers).announce
}