package org.georgatos.torree.core

import org.georgatos.torree.parsers.MagnetParser
import org.georgatos.torree.util._

import scala.annotation.tailrec

case class MagnetLink(potentialMagnetString: String) {
  val ValidMagnetR  = """(?i)magnet:\?(xt=urn:[a-z0-9]+:[a-z0-9]{32}.*?)\&(.*)""".r

  lazy val ValidMagnetR(_urn, _meta) = potentialMagnetString

  def meta: String = _meta

  def urn: String  = _urn

  def dn: Option[List[String]] = parsedMagnetLink match {
    case Some(hashMap) => hashMap.get("dn")
    case _ => None
  }

  def name: Option[String] = dn match {
    case Some(Decode.utf8(head) :: _) => Some(head)
    case _ => None
  }

  def trackers: Option[List[String]] = parsedMagnetLink match {
    case Some(hashMap) => hashMap.get("tr")
    case _ => None
  }

  def valid: Boolean = potentialMagnetString match {
    case ValidMagnetR(_, _) => true
    case _ => false
  }

  def parsedMagnetLink = MagnetParser(this)
}
