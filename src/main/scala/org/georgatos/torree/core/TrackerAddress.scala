package org.georgatos.torree.core

import org.georgatos.torree.util.Decode

abstract class AddressProtocol {
  type A <: AddressProtocol
  abstract def validate: Boolean
}
class UDP extends AddressProtocol {
  /**
    * UDP address regular expression
    */
  val addressCheck = """udp:\/\/([^:\/]*)(?::([0-9]*))?(?:\/)?""".r

  override def validate: Boolean = {
    false
  }
}
class HTTP extends AddressProtocol {
  /**
    * HTTP address regular expression
    */
  val addressCheck = """http:\/\/([^:\/]*)(?::([0-9]*))?(?:\/)?""".r

  override def validate: Boolean = {
    true
  }
}

class TrackerAddress[A](address: String) extends AddressProtocol {
  def unapply[A](x: String) = apply(x).validate
  def apply[A](x: String) = new TrackerAddress[A](address)
}


object TrackerAddress {
  def apply(address: String) = address match {
    case TrackerAddress[UDP](x) => new TrackerAddress[UDP](str)
    case TrackerAddress[HTTP](x) => new TrackerAddress[HTTP](str)
  }
}