package org.georgatos.torree.parsers

import org.georgatos.torree.core.MagnetLink
import org.georgatos.torree.exceptions._
import scala.collection.immutable.HashMap

class MagnetParser private (val _magnetLink: MagnetLink) {
  def splitToEqualSign(str: String) = str.split("=") match {
    case Array(k: String, v: String, _*) => (k, v)
  }

  def updateHashMap(hashMap: HashMap[String, List[String]], r: (String, String)): HashMap[String, List[String]] = {
    hashMap.updated(r._1, hashMap.getOrElse(r._1, List()) ++ List(r._2))
  }

  def parse: Option[HashMap[String, List[String]]] = {
    Some(_magnetLink.meta.split("&").map(splitToEqualSign)
      .foldLeft(HashMap[String, List[String]]())((hash, r) => updateHashMap(hash, r)))
  }
}

object MagnetParser {
  def apply(magnetLink: MagnetLink): Option[HashMap[String, List[String]]] = {
    if (magnetLink.valid) new MagnetParser(magnetLink).parse
    else throw new InvalidMagnetString("Invalid String")
  }
}
