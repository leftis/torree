package org.georgatos.torree.cli

import org.clapper.argot._
import org.georgatos.torree.core.{Tracker, MagnetLink}
import ArgotConverters._

object Parser {
  val parser = new ArgotParser("torree", compactUsage=true, preUsage=Some("Torree, Version 0.1 Copyright (c) 2015, Lefteris Georgatos."))
  val magnet = parser.parameter[String]("magnet_uri", "Movie's magnet string.", false)

  def run = {
    magnet.value match {
      case Some(r) if MagnetLink(r).valid => Tracker(MagnetLink(r).trackers)
      case _ => println("Magnet URI is invalid, please try again.")
    }
  }

  def main(args: Array[String]) {
    try {
      parser.parse(args)
      run
      parser.reset()
    } catch {
      case e: ArgotUsageException => println(e.message)
    }
  }
}